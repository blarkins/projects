﻿using System.Data;

namespace LS_POC.Data
{
    public class DataFetch
    {
        public int PropertyID { get; set; }
        public string PropertyType { get; set; }
        public string PropertyStatus { get; set; }
        public string PropertyName { get; set; }
        public string HospitalAfilliation { get; set; }
        public string SystemName { get; set; }
        public string BuildingQuality { get; set; }
        public string OwnershipType { get; set; }
        public string Owner { get; set; }
        public int YearBuilt { get; set; }
        public int Stories { get; set; }
        public int SquareFeet { get; set; }
        public DateTime LastTransactionDate { get; set; }

       
			public string Address { get; set; }//
			public string City { get; set; }//
			public string StateProvince { get; set; }//
			public string Zip5 { get; set; }//
			public string CountyName { get; set; }//
		
			public double Lat { get; set; }//
			public double Lon { get; set; }//
			
			public string IsHospitalAffiliated { get; set; }//
			public string IsSystemAffiliated { get; set; }//
			public string IsAffiliated { get; set; }//
			public short ProviderCount { get; set; }//
			public short PracticeCount { get; set; }//
			public string Tenancy { get; set; }//
			public string OnCampus { get; set; }//
			
			public string AskingRent { get; set; }//
			public string AskingRentType { get; set; }//
			public string TotalSpaceAvailable { get; set; }//
			
			public decimal PPSF { get; set; }//
			public string BuyerName { get; set; }//
			
			public string Link { get; set; }//
		}




	
}
