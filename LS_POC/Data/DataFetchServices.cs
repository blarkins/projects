﻿using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace LS_POC.Data
{
    public class DataFetchServices
    {
    

        public static List<DataFetch> FetchData()
        {
            IConfigurationRoot config = new ConfigurationBuilder()
                     .AddJsonFile("appsettings.json")
                     .Build();
            var dataFetchlist = new List<DataFetch>();
            var dataFetch = new DataFetch();
            string[] sqlparam = { "100", "102", "105", "107", "109", "110", "111", "112" };

            string conn = config.GetConnectionString("DWConnectionString");
            using (IDbConnection db = new SqlConnection(conn))
            {
                foreach (var item in sqlparam)
                {
                    dataFetch = db.Query<DataFetch>("[DW].[Select_PropertySummary_ByList]", new
                    {
                        PropertyIDList = item
                    },
                                       commandType: CommandType.StoredProcedure).FirstOrDefault();
                    dataFetchlist.Add(dataFetch);
                  
                }
            }

            //result is list of CustomTest
            return dataFetchlist;
            
        }


    }
}
